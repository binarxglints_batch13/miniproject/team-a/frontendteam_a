import React from 'react'
import logo from '../images/logo.png'

export default function Header() {
    return (
        <div>
           <nav className="navbar navbar-light">
                <div className="container">
                    <a className="navbar-brand d-flex align-items-center" href="#">
                        <img src={logo} alt="logo"/>
                        <h2 className="ps-1">MilanTV</h2>
                    </a>
                    <form>
                        <input className="form-control" type="text" placeholder="Search" />
                    </form>
                    <a><h2>Sign In</h2></a>
                </div>
            </nav>

        </div>
    )
}
