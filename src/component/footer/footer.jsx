import React from 'react'
import logo from '../images/logo.png'
import facebook from './images/facebook.png'
import googlePlay from './images/google-play.png'
import appleStore from './images/apple-store.png'
import pinterest from './images/pinterest.png'
import instagram from './images/instagram.png'

export default function Footer() {
    return (
        <div className="bg-dark text-light pt-3">
            <div className="container d-flex border-bottom">
                <div className="container">
                    <a href="#" className="navbar-brand">
                        <div className="text-light d-flex align-items-center">
                            <img src={logo} alt="logo" />
                            <h2 className="ps-1">MilanTV</h2>
                        </div>
                        
                    </a>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
                </div>
                <div className="container d-flex justify-content-center">
                    <div className="d-flex flex-column">
                        <p>Tentang Kami</p>
                        <p>Blog</p>
                        <p>Layanan</p>
                        <p>Karir</p>
                        <p>Pusat Media</p>
                    </div>
                </div>
                <div className="container">
                    <h4>Download</h4>
                    <div className="d-flex mb-3">
                        <a href="#"><img src={googlePlay}></img></a>
                        <a href="#"><img src={appleStore}></img></a>
                    </div>
                    <h4>Social media</h4>
                    <div className="d-flex">
                        <a href="#" className="me-3"><img src={facebook} alt="" /></a>
                        <a href="#" className="me-3"><img src={instagram} alt="" /></a>
                        <a href="#"><img src={pinterest} alt="" /></a>
                    </div>
                </div>
            </div>
            <div className="p-3 d-flex justify-content-center">
                <p>Copyright © 2000-202 MilanTV.  All Rights Reserved</p>
            </div>
        </div>
    )
}
